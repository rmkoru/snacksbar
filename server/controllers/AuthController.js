const express = require('express');
let auth = {};

auth.userLogin = function(req, res){
    res.json({"msg": "User Logged In"});
}

auth.userLogout = function(req, res){
    res.json({"msg": "User Logged Out"});
}

module.exports = auth;