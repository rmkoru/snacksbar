const express = require("express");
const router = express.Router();
const AuthController = require("./controllers/AuthController.js");
const UserController = require("./controllers/UsersController.js");

router.get('/auth/login', function(req, res){
    AuthController.userLogin(req, res)
})

router.get('/auth/logout', function(req, res){
    AuthController.userLogout(req, res)
})

router.get('/users', function(req, res){
    UserController.getUsers(req, res)
})

router.get('/*', function(req, res){
    res.json({"msg": "Page Not Found!!!"})
})


module.exports = router;